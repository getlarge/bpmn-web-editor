'use strict';

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  var path = require('path');

  /**
   * Resolve external project resource as file path
   */
  function resolvePath(project, file) {
    return path.join(path.dirname(require.resolve(project)), file);
  }

  // project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    config: {
      sources: 'app',
      dist: 'dist',
      assets: 'assets'
    },

    browserify: {
      options: {
        browserifyOptions: {
          debug: true,
          list: true,
          // strip unnecessary built-ins
          builtins: [ 'events' ],
          insertGlobalVars: {
            process: function () {
                return 'undefined';
            },
            Buffer: function () {
                return 'undefined';
            }
          }
        },
        transform: [ 'brfs' ]
      },
      watch: {
        options: {
          watch: true
        },
        files: {
          '<%= config.dist %>/viewer.js': [ '<%= config.sources %>/viewer.js' ],
          '<%= config.dist %>/modeler.js': [ '<%= config.sources %>/modeler.js' ],
          '<%= config.dist %>/modeler_test.js': [ '<%= config.sources %>/modeler_test.js' ]
        }
      },
      app: {
        files: {
          '<%= config.dist %>/viewer.js': [ '<%= config.sources %>/viewer.js' ],
          '<%= config.dist %>/modeler.js': [ '<%= config.sources %>/modeler.js' ],
          '<%= config.dist %>/modeler_test.js': [ '<%= config.sources %>/modeler_test.js' ]
        }
      }
    },

    copy: {
      app: {
        files: [
          {
            expand: true,
            cwd: '<%= config.sources %>/',
            src: ['**/*.*', '!**/*.js'],
            dest: '<%= config.dist %>'
          },
          {
            expand: true,
            cwd: resolvePath('bpmn-js', 'assets'),
            src: ['**/*.*', '!**/*.js'],
            dest: '<%= config.dist %>/vendor'
          },
          {
            src: resolvePath('bpmn-js-embedded-comments', 'assets/comments.css'),
            dest: '<%= config.dist %>/css/comments.css'
          },
          {
            src: resolvePath('diagram-js', 'assets/diagram-js.css'),
            dest: '<%= config.dist %>/css/diagram-js.css'
          },
          {
            expand: true,
            cwd: resolvePath('bpmn-js-token-simulation', 'assets'),
            src: ['**/*.*'],
            dest: '<%= config.dist %>/css'
          },
          {
            expand: true,
            cwd: '<%= config.assets %>/',
            src: ['**/*.*'],
            dest: '<%= config.dist %>'
          }
        ]
      }
    },

    less: {
      options: {
        dumpLineNumbers: 'comments',
        paths: [
          'node_modules'
        ]
      },

      styles: {
        files: {
          'dist/css/properties-panel.css': 'styles/properties-panel.less',
          'dist/css/comments-styles.css': 'styles/comments-styles.less'
        }
      }
    },

    watch: {
      options: {
        livereload: true
      },
      samples: {
        files: [ 
          '<%= config.sources %>/**/*.*',
          '<%= config.assets %>/**/*.*'
        ],
        tasks: [ 'copy:app' ]
      },
      less: {
        files: [
          'styles/**/*.less',
          'node_modules/bpmn-js-properties-panel/styles/**/*.less'
        ],
        tasks: [
          'less'
        ]
      },
    },

    connect: {
      livereload: {
        options: {
          port: 9013,
          livereload: true,
          hostname: 'localhost',
          open: true,
          base: [
            '<%= config.dist %>'
          ]
        }
      }
    }
  });

  // tasks

  grunt.registerTask('build', [ 'browserify:app', 'copy:app' ]);

  grunt.registerTask('auto-build', [
    'copy',
    'less',
    'browserify:watch',
    'connect:livereload',
    'watch'
  ]);

  grunt.registerTask('default', [ 'build' ]);
};
